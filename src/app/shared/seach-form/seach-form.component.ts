import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-seach-form',
  templateUrl: './seach-form.component.html',
  styleUrls: ['./seach-form.component.scss']
})
export class SeachFormComponent implements OnInit {

  @Output() ChangedOn = new EventEmitter<any>();

  search = '';

  constructor() { }

  ngOnInit() {
  }

  setSearch() {
    this.ChangedOn.emit(this.search);
  }

}
