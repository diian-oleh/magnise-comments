import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder, } from '@angular/forms';
import { Validators } from '@angular/forms';

@Component({
  selector: 'app-comment-form',
  templateUrl: './comment-form.component.html',
  styleUrls: ['./comment-form.component.scss']
})
export class CommentFormComponent implements OnInit {
  @Input() parentId: string;
  @Output() AddComment = new EventEmitter<any>();

  submited = false;

  commentForm = this.fb.group({
    content: ['', Validators.required],
  });

  constructor(private fb: FormBuilder) { }

  ngOnInit() {
  }

  onSubmit() {
    this.submited = true;
    if (this.commentForm.valid) {
      const data = this.commentForm.value;
      data.parentId = this.parentId || null;
      this.AddComment.emit(data);
      this.commentForm.reset();
      this.submited = false;
    }
  }
}
