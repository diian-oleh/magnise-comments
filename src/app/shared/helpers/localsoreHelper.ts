export default class LocalStorage {


  public static getItem($key: string) {
    try {
      const item = localStorage.getItem($key);
      try {
        const jsonItem = JSON.parse(item);
        return jsonItem;
      } catch (error) {
        return item;
      }
    } catch (error) {
      return false;
    }
  }


  public static setItem($key: string, $value: any) {
    try {
      if (Array.isArray($value) || $value instanceof Object) {
        const json = JSON.stringify($value);
        localStorage.setItem($key, json);
      } else {
        localStorage.setItem($key, $value);
      }
      return true;
    } catch (error) {
      return false;
    }
  }


  public static pushItem($key: string, $value: any) {
    const arrStore = this.getItem($key);
    if (Array.isArray(arrStore)) {
      arrStore.push($value);
      return true;
    } else {
      return false;
    }
  }

  public static deleteItem($key: string) {
    try {
      localStorage.removeItem($key);
      return true;
    } catch (error) {
      return false;
    }
  }

}
