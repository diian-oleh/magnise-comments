export interface Comment {
  id: string;
  content: string;
  parentId?: string;
}

export function genCommentId(): string {
  const haracters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  return '_' + Math.random().toString()
    + haracters.charAt(Math.floor(Math.random() * haracters.length))
    + new Date().getMilliseconds();
}
