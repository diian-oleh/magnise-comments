import { Comment, genCommentId } from './comment.model';

export default (function () {
  const comments: Array<Comment> = [];

  for (let index = 1; index < 4; index++) {
    const comment: Comment = {
      id: genCommentId(),
      content: 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Nostrum, aliquid.',
      parentId: null
    };
    comments.push(comment);
  }
  return comments;
})();
