import {
  Component, OnInit
} from '@angular/core';
import {
  trigger, transition,
  style, animate, query, stagger
} from '@angular/animations';


import { Comment } from './shared/models/comment.model';
import { CommentService } from './core/services/comment.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [
    trigger('commentsAnimation', [
      transition('* => *', [
        query(':leave', [
          stagger(100, [
            animate('500ms', style(
              { opacity: 0, transform: 'scale(0)' }))
          ])
        ], { optional: true }),
        query(':enter', [
          style({ opacity: 0, transform: 'scale(1)' }),
          stagger(100, [
            animate('500ms', style({ opacity: 1 }))
          ])
        ], { optional: true })
      ])
    ])
  ],
})

export class AppComponent implements OnInit {
  title = 'magnise-comments';
  comments: any = [];
  commentsData = [];
  search: '';

  constructor(
    private commentService: CommentService) { }

  ngOnInit() {
    this.getComments();
  }

  getComments(): void {
    this.commentService.getComments().subscribe((data: Array<Comment>) => {
      this.commentsData = data;
      this.comments = this.structurizeComments(data);
    });
  }

  addComment($event) {
    this.commentService.addComment($event);
  }

  delete(id: string) {
    this.commentService.delete(id);
  }

  setSearch($event) {
    this.search = $event;
  }

  private structurizeComments(data: Array<Comment>) {

    const structurize = function (array: Array<Comment>, parent?: any, tree?: any) {
      tree = typeof tree !== 'undefined' ? tree : [];
      parent = typeof parent !== 'undefined' ? parent : { id: null };

      const children = array.filter(function (child) {
        return child.parentId === parent.id;
      });
      if (children.length) {
        if (!parent.id) {
          tree = children;
        } else {
          parent['children'] = children;
        }
        children.forEach((child) => {
          structurize(array, child);
        });

      }
      return tree;
    };
    //  delete reference of array
    const comments = structurize(JSON.parse(JSON.stringify(data)));

    return comments;

  }

}
