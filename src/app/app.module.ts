import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { SeachFormComponent } from './shared/seach-form/seach-form.component';
import { CommentFormComponent } from './shared/comment-form/comment-form.component';
import { FilterPipe } from './core/pipes/filter.pipe';


@NgModule({
  declarations: [
    AppComponent,
    SeachFormComponent,
    CommentFormComponent,
    FilterPipe,

  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
