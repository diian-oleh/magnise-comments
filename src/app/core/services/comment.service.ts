import { Injectable } from '@angular/core';

import { Observable, of, Subject, BehaviorSubject } from 'rxjs';
import fakeCommentsData from '../../shared/models/fakeData';
import { Comment, genCommentId } from '../../shared/models/comment.model';


@Injectable({
  providedIn: 'root'
})
export class CommentService {

  private fakeComments = new BehaviorSubject<Array<Comment>>([]);

  constructor() {
    this.fakeComments.next(fakeCommentsData);
  }

  getComments(): Observable<Array<Comment>> {
    return this.fakeComments.asObservable();
  }

  addComment(data: { content: string, parentId: string }): void {
    const comments = this.fakeComments.getValue();

    const newComment: Comment = {
      id: genCommentId(),
      content: data.content,
      parentId: data.parentId
    };
    comments.push(newComment);
    this.fakeComments.next(comments);
  }

  delete(id: string): void {
    const comments = this.fakeComments.getValue();

    const newComments = comments.filter((data: Comment) => {
      return data.id !== id;
    });
    this.fakeComments.next(newComments.slice());
  }

}
